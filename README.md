# README #

FIRE stands for Feynman Integral REduction

### Articles ###

* [FIRE3](http://arxiv.org/abs/0807.3243)
* [FIRE4](http://arxiv.org/abs/1302.5885)
* [FIRE5](http://arxiv.org/abs/1408.2372)
* [FIRE6](http://arxiv.org/abs/1901.07808)

### Installation ###

Either download a binary package, or

* git clone https://bitbucket.org/feynmanIntegrals/fire.git
* cd fire/FIRE6
* ./configure
* Now read the options provided by ./configure and reconfigure with desired options, for example
* ./configure --enable_zlib --enable_snappy --enable_lthreads --enable_tcmalloc --enable_zstd
* make dep
* make

FIRE works under various Linux distributions.
FIRE works under Windows inside the WSL, version 2 (Windows subsystem for Linux). Just get WSL with an Ubuntu installation.

Important notice! We ship LiteRed 1.8.3 with FIRE. It is a separate package created by R.N.Lee, and in case it is used, a paper on LiteRed should be cited together with FIRE.

In case of changes in ./configure options it is recommended to have a clean rebuild

* make cleandep
* make clean
* make dep
* make

### Usage ###

* make test
* Follow the instructions in the articles
* There are some examples in the examples folder

For details of installation under OSX see [the following issue](https://bitbucket.org/feynmanIntegrals/fire/issues/10/issue-of-the-installation-on-macos) thanks to Sudeepan Datta

### Documentation ###

Doxygen is used to create documentation for FIRE.
You need to have _doxygen_ installed to generate documentation.

To generate docs run

* make doc

This will create _html/_ and _latex/_ subfolders in _FIRE6/documentation/_
_html/_ contains complete docs, _latex/_ contains latex sources.

To generate .pdf from latex sources, run

* make doc_pdf

You will need to have appropriate tools installed, like _pdflatex_.
This will generate _refman.pdf_ and place it directly in _FIRE6/documentation/_

To view docs after creation, either

* open _FIRE6/documentation/html/index.html_ in your Web Browser
* open _FIRE6/documentation/refman.pdf_ (after generating it)

To delete documentation run

* make cleandoc

### More information ###

* For the package structure see FIRE6/README
* For examples listing see FIRE6/examples/README
* For information about documentation see FIRE6/documentation/README

### External packages ###

* Most of the packages that FIRE uses are open-source, so they are included in the FIRE distribution
* FIRE relies on the [Fermat](https://home.bway.net/lewis/) program by Robert Lewis. Fermat is free-ware, but has some restrictions for organizations. Fermat is shipped in the FIRE package, however it is the user responsibility to check, whether his use of Fermat is legal. If one does not accept the Fermat license, he should not use the C++ FIRE as well.
* Suggested usage is together with [LiteRed](http://www.inp.nsk.su/~lee/programs/LiteRed/). Do not forget to include a reference to https://arxiv.org/abs/1310.1145 in this case.
