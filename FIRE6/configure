#!/bin/bash

echo "$*" | tr _ - > previous_options

echo "";
echo "This script is not a classical configure script.";
echo "It only creates the Makefile according to the options you pass.";
echo "Changes in some options require rebuilding of dependancy libraries (make cleandep && make dep) first.";
echo "The following options are valid:";
echo "";
echo "--enable_zlib: enables the zlib compressor shipped with kyotocabinet, requires the zlib/deflate library (zlib1g-dev)";
echo "--enable_snappy: tries to build the snappy library shipped with FIRE (might require some automake magic)";
echo "--enable_zstd: tries to build the ZStandard compressor";
echo "--enable_debug: add -g to compile options for debugging symbols";
echo "--enable_tcmalloc: use tcmalloc from google perf tools instead of the libc malloc"
echo "--enable_lthreads: enable multiple threads inside a sector (separated by levels): the #lthreads option in config"
echo "--disk_database: switch to the disk database mode instead of RAM database"
echo "--small_point: make point size 16 bytes instead of 24"
echo "-----------------------------------------------------------------------------------------------------------";
echo ""
echo "Now creating Makefile";

enable_zlib=0
enable_snappy=0
enable_debug=0
enable_tcmalloc=0
enable_lthreads=0
enable_zstd=0
disk_database=0
small_point=0
for var in "$@"
do
    if [ $var = "--enable-zlib" ]; then enable_zlib=1; fi
    if [ $var = "--enable-snappy" ]; then enable_snappy=1; fi
    if [ $var = "--enable-debug" ]; then enable_debug=1; fi
    if [ $var = "--enable-tcmalloc" ]; then enable_tcmalloc=1; fi
    if [ $var = "--enable-lthreads" ]; then enable_lthreads=1; fi
    if [ $var = "--enable-zstd" ]; then enable_zstd=1; fi
    if [ $var = "--enable_zlib" ]; then enable_zlib=1; fi
    if [ $var = "--enable_snappy" ]; then enable_snappy=1; fi
    if [ $var = "--enable_debug" ]; then enable_debug=1; fi
    if [ $var = "--enable_tcmalloc" ]; then enable_tcmalloc=1; fi
    if [ $var = "--enable_lthreads" ]; then enable_lthreads=1; fi
    if [ $var = "--enable_zstd" ]; then enable_zstd=1; fi
    if [ $var = "--disk_database" ]; then disk_database=1; fi
    if [ $var = "--disk-database" ]; then disk_database=1; fi
    if [ $var = "--small_point" ]; then small_point=1; fi
    if [ $var = "--small-point" ]; then small_point=1; fi
done
paths=$(cat paths.inc.in)
text=`cat Makefile.in`
stext=`cat poly/Makefile.in`
ptext=`cat prime/Makefile.in`
mtext=`cat mpi/Makefile.in`
if [ $enable_zlib = 1 ]; then echo "Enabling zlib compressor";
    text=${text/"--disable-zlib "/""}; else
    stext=${stext/"-DWITH_ZLIB "/""};
    ptext=${ptext/"-DWITH_ZLIB "/""};
    stext=${stext/"-lz "/""};     stext=${stext/"-lz "/""};
    ptext=${ptext/"-lz "/""};    ptext=${ptext/"-lz "/""};
fi
if [ $small_point = 1 ]; then echo "Using small point size"; else
    stext=${stext/"-DSMALL_POINT"/""};
    ptext=${ptext/"-DSMALL_POINT"/""};
fi 
if [ $disk_database = 1 ]; then echo "Switching to disk database mode"; else
    stext=${stext/"-DDISK_DB"/""};
    ptext=${ptext/"-DDISK_DB"/""};
fi
if [ $enable_debug = 1 ]; then echo "Enabling debug symbols"; else
    stext=${stext/"-g "/""};
    ptext=${ptext/"-g "/""};
    mtext=${mtext/"-g "/""};
    stext=${stext/"-rdynamic "/""};
    ptext=${ptext/"-rdynamic "/""};
    mtext=${mtext/"-rdynamic "/""};
    stext=${stext/"-rdynamic "/""};
    ptext=${ptext/"-rdynamic "/""};
    mtext=${mtext/"-rdynamic "/""};
    stext=${stext/"-DWITH_DEBUG "/""};
    ptext=${ptext/"-DWITH_DEBUG "/""};
    mtext=${mtext/"-DWITH_DEBUG "/""};
fi
if [ $enable_snappy = 1 ]; then echo "Enabling snappy compressor"; else
    stext=${stext/"-DWITH_SNAPPY "/""};
    stext=${stext//"-lsnappy "/""};
    ptext=${ptext/"-DWITH_SNAPPY "/""};
    ptext=${ptext//"-lsnappy "/""};
    text=${text/"cd extra/snappy-1.1.7/ && mkdir -p build && cd build && cmake ../"/""};
    text=${text/"\$(MAKE) -C ./extra/snappy-1.1.7/build DESTDIR=../../../ install"/""};
    text=${text/"\$(MAKE) -C ./extra/snappy-1.1.7/build"/""};
fi
if [ $enable_zstd = 1 ]; then echo "Enabling ZStandard compressor"; else
    stext=${stext/"-DWITH_ZSTD "/""};
    stext=${stext//"-lzstd "/""};
    ptext=${ptext/"-DWITH_ZSTD "/""};
    ptext=${ptext//"-lzstd "/""};
    text=${text/"\$(MAKE) -C ./extra/zstd-1.4.3 PREFIX=\`pwd\`/usr/ uninstall"/""};
    text=${text/"\$(MAKE) -C ./extra/zstd-1.4.3 PREFIX=\`pwd\`/usr/ install"/""};
    text=${text/"\$(MAKE) -C ./extra/zstd-1.4.3"/""};
fi
if [ $enable_tcmalloc = 1 ]; then echo "Enabling tcmalloc"; else
    stext=${stext/"-fno-builtin-malloc -fno-builtin-calloc -fno-builtin-realloc -fno-builtin-free"/""};
    stext=${stext//"-ltcmalloc_minimal "/""};
    ptext=${ptext/"-fno-builtin-malloc -fno-builtin-calloc -fno-builtin-realloc -fno-builtin-free"/""};
    ptext=${ptext//"-ltcmalloc_minimal "/""};
    paths=${paths/"ENABLE_TCMALLOC=1"/""}
fi
if [ $enable_lthreads = 1 ]; then
    echo "Enabling level threads";
    cp extra/kyotocabinet-1.2.77/kccachedb.h.threadsafe.in extra/kyotocabinet-1.2.77/kccachedb.h
    else
    cp extra/kyotocabinet-1.2.77/kccachedb.h.in extra/kyotocabinet-1.2.77/kccachedb.h
    stext=${stext/"-DFSBALLOCATOR_USE_THREAD_SAFE_LOCKING_PTHREAD "/""};
    ptext=${ptext/"-DFSBALLOCATOR_USE_THREAD_SAFE_LOCKING_PTHREAD "/""};
fi

echo "$paths" >paths.inc
echo "$text" > Makefile
echo "$stext" > poly/Makefile
echo "$ptext" > prime/Makefile
echo "$mtext" > mpi/Makefile

echo "";
echo "Done";
echo "make dep: build dependancy libraries";
echo "make: build FIRE";
echo "";
echo "";
echo "Minimal gcc version required: 4.8.1"
echo "Current gcc version: $(gcc -dumpversion)"
echo "";
echo "";
